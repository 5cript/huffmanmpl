#include "main.hpp"

#include "huffman.hpp"
#include "debug.hpp"

#include <iostream>
#include <fstream>

/** This project does not aim to be "industrial strength" level.
 *  It has not been intended to be implemented into other projects (e.g. no namespace)
 */

// unfortunately: the text that is to be analysed has a maximum size.
// I could make this configurable, but:
// 1) It was not worth the effort for this funny project.
// 2) If this were to be configured too high, I would potentially break the compilers maximum recursion depth (which is very low anyway; 500 on gcc i think)
//    and would have to be increased by compiler flags.
//
// If you use a lot of symbols, the amount of needed RAM will sky rocket!

int main()
{
    // lets analyse some very fitting latin sayings - just for fun:
    ANALYSE_TEXT (
        "absens haeres non erit." // Out of sight, out of mind.
        "abyssus abyssum invocat." // Deep calls to deep.
        "consuetudinis magna vis est." // old habits die hard
    ) AS latin;

    PRINT_HUFFMAN_CODE (latin, std::cout);
}

void testEncoding()
{
// excluded from compilation to save time
/*
    // used for testing:
    ANALYSE_TEXT (
        "abbcccddddeeeeeffffff"
    ) AS simpleTestCase;

    PRINT_HUFFMAN_CODE (simpleTestCase, std::cout);

    std::cout << "\n--------------------------------------------\n\n";
*/
}

void testLimits()
{
// excluded from compilation to save time
/*
    ANALYSE_TEXT (
        "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,;/+*-"
    ) AS limiter;
*/
}

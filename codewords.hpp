#ifndef CODEWORDS_H_INCLUDED
#define CODEWORDS_H_INCLUDED

#include "symbolex.hpp"
#include "symbolcounter.hpp"
#include "mplincludes.hpp"

#include <string>

template <  typename Sequence /* mpl sequence */,
            unsigned Multiplier = 10000, /* higher means higher precision (100 = pecent, 1000 = permille, ...) */
            unsigned Accumulated = mpl::accumulate <Sequence, mpl::long_<0>, mpl::plus <mpl::_, mpl::_>>::type::value
         >
struct CalculateProbabilty {
    using _ = mpl::placeholders::_;

    using type = typename
    mpl::transform <Sequence, mpl::divides <mpl::multiplies <_, mpl::int_<Multiplier>>, mpl::long_<Accumulated>>>::type;
};

template <char SymbolV, unsigned CountV>
struct Codeword
{
    struct Symbol { static constexpr char value = SymbolV; };
    struct Count { static constexpr unsigned value = CountV; };

    using type = Codeword <SymbolV, CountV>;
};

template <char SymbolV, typename CodeSequenceT>
struct CompiledCodeword
{
    struct Symbol { static constexpr char value = SymbolV; };
    struct Code { using type = CodeSequenceT; };
};

template <typename Symbols, typename OccurenceCount>
class CodewordCoupler
{
private:
    using _ = mpl::_;

    template <typename Sequence, typename I_Symbols, typename I_OccurenceCount>
    struct CoupleInserter
    {
        using type = typename
        mpl::push_back <Sequence,
                        Codeword <mpl::deref<typename mpl::begin<I_Symbols>::type>::type::value,
                                        mpl::deref<typename mpl::begin<I_OccurenceCount>::type>::type::value>
                        >::type;
    };

    template <typename Sequence, typename I_Symbols, typename I_OccurenceCount,
              typename Enabler = void>
    struct Unroller
    {
        using type = typename
        Unroller <
            typename CoupleInserter <Sequence, I_Symbols, I_OccurenceCount>::type,
            typename mpl::pop_front <I_Symbols>::type,
            typename mpl::pop_front <I_OccurenceCount>::type
        >::type;
    };

    template <typename Sequence, typename I_Symbols, typename I_OccurenceCount>
    struct Unroller <Sequence, I_Symbols, I_OccurenceCount, typename std::enable_if <mpl::empty<I_Symbols>::type::value>::type>
    {
        using type = Sequence;
    };

public:
    using type = typename
    Unroller <std::tuple <>, Symbols, OccurenceCount>::type;
};

template <typename CompiledCodewordT>
struct CodeToString
{
    static void compile(std::string& str)
    {
        str.push_back('0' + mpl::front <typename CompiledCodewordT::Code::type>::type::value);

        CodeToString <
            CompiledCodeword <
                CompiledCodewordT::Symbol::value,
                typename mpl::pop_front <typename CompiledCodewordT::Code::type>::type
            >
        >::compile(str);
    };
};

template <char Symbol>
struct CodeToString <CompiledCodeword <Symbol, std::tuple<>>>
{
    static void compile (std::string& str) {}
};

template <typename CodewordListT>
struct CcwPrinter
{
    static void print(std::ostream& stream)
    {
        using CompiledCodewordT = typename mpl::front <CodewordListT>::type;

        std::string str {};

        // transforms bool tuple into string
        CodeToString<CompiledCodewordT>::compile(str);

        // pretty prints current symbol and code
        stream << CompiledCodewordT::Symbol::value << ": " << str << "\n";

        CcwPrinter <typename mpl::pop_front <CodewordListT>::type>::print(stream);
    }
};

template <>
struct CcwPrinter <std::tuple<>>
{
    static void print(std::ostream& stream) {}
};

#define GET_STR_AUX(_, i, str) (sizeof(str) > (i)+1 ? str[sizeof(str)-(i)-2] : 0),
#define GET_STR(str) BOOST_PP_REPEAT(128,GET_STR_AUX,str) 0

#define ANALYSE_TEXT(STRING) \
    typedef CodewordCoupler<TextWrapper<GET_STR(STRING)>::SymbolTable::type, SymbolCounter<TextWrapper<GET_STR(STRING)>>::type>::type

#define AS

#endif // CODEWORDS_H_INCLUDED

#ifndef HUFFMAN_HPP_INCLUDED
#define HUFFMAN_HPP_INCLUDED

#include <utility>
#include <type_traits>

#include <boost/preprocessor/repetition/repeat.hpp>

#include "codewords.hpp"

template <typename LhsT, // another node or a leaf
          typename RhsT, // another node or a leaf
          unsigned CountV // the occurence count or probability (whichever you like to use)
         >
struct BinaryTreeNode
{
    struct Left { using type = LhsT; }; // 1 side
    struct Right { using type = RhsT; }; // 0 side

    // Count is an internal convention for the probabilty
    struct Count { static constexpr unsigned value = CountV; };

    using type = BinaryTreeNode<LhsT, RhsT, CountV>;
};

//####################################################################################################################################

template <typename LhsT, typename RhsT>
struct SortPredicate
{
    using type = typename
    mpl::bool_ <(LhsT::Count::value < RhsT::Count::value)>;

    static constexpr unsigned value = type::value;
};

//####################################################################################################################################

// the used algorithm has O(n log n) runtime
template <typename TreeT, // i do expect this tree to always be sorted before insertion
          typename Enabler = void>
class NodeFactory
{
private:
    using first = typename mpl::front <TreeT>::type;
    using second = typename mpl::front <typename mpl::pop_front <TreeT>::type>::type;
    using reducedTree = typename mpl::pop_front <typename mpl::pop_front <TreeT>::type>::type;

    using node =
    BinaryTreeNode <first,
                    second,
                    first::Count::value + second::Count::value>;

public:
    using type = typename
    NodeFactory <
        typename mpl::sort <
            typename mpl::push_back <reducedTree, node>::type,
            SortPredicate<mpl::_,mpl::_>
        >::type
    >::type;
};

template <typename TreeT>
class NodeFactory <TreeT, typename std::enable_if <mpl::size <TreeT>::type::value == 1, void>::type>
{
public:
    using type = typename mpl::at <TreeT, mpl::int_<0>::type>::type;
};

//####################################################################################################################################

template <typename TreeT,
          typename CodeAccumulatorT>
struct CodeGenerator
{
    using type = TreeT; // which is a codeword

    struct Code { using type = CodeAccumulatorT; };
    struct IsNode { using type = mpl::bool_<0>::type; };
    struct IsLeaf { using type = mpl::bool_<1>::type; };
};

template <typename LhsT, typename RhsT, unsigned CountV,
          typename CodeAccumulatorT>
struct CodeGenerator <BinaryTreeNode<LhsT,RhsT,CountV>, CodeAccumulatorT>
{
    struct Left // subtree
    {
        using type = typename
        CodeGenerator <
            typename BinaryTreeNode<LhsT,RhsT,CountV>::Left::type,
            typename mpl::push_back <CodeAccumulatorT, mpl::bool_<1>::type>::type
        >::type;

        struct Self // hack, don't touch
        {
            using type =
            CodeGenerator <
                typename BinaryTreeNode<LhsT,RhsT,CountV>::Left::type,
                typename mpl::push_back <CodeAccumulatorT, mpl::bool_<1>::type>::type
            >;
        };

    };
    struct Right // subtree
    {
        using type = typename
        CodeGenerator <
            typename BinaryTreeNode<LhsT,RhsT,CountV>::Right::type,
            typename mpl::push_back <CodeAccumulatorT, mpl::bool_<0>::type>::type
        >::type;

        struct Self // hack, don't touch
        {
            using type =
            CodeGenerator <
                typename BinaryTreeNode<LhsT,RhsT,CountV>::Right::type,
                typename mpl::push_back <CodeAccumulatorT, mpl::bool_<0>::type>::type
            >;
        };
    };
    struct Code { using type = CodeAccumulatorT; };
    struct IsNode { using type = mpl::bool_<1>::type; };
    struct IsLeaf { using type = mpl::bool_<0>::type; };

    using type = typename BinaryTreeNode<LhsT,RhsT,CountV>::type; // which is a binary tree node
};

//####################################################################################################################################

template <typename GeneratorT,
          typename TreeT>
class CodeExtractor
{
private:
    using rhs = typename CodeExtractor <typename GeneratorT::Right::Self::type, typename TreeT::Right::type>::type;
    using lhs = typename CodeExtractor <typename GeneratorT::Left::Self::type, typename TreeT::Left::type>::type;

public:
    using type = typename
    mpl::copy <
        rhs,
        mpl::back_inserter <lhs>
    >::type;
};

template <typename GeneratorT, char Symbol, unsigned Count>
class CodeExtractor <GeneratorT, Codeword<Symbol, Count>>
{
public:
    using type = std::tuple <CompiledCodeword <Symbol, typename GeneratorT::Code::type>>;
};

//####################################################################################################################################

template <typename Codewords>
class HuffmanTree
{
private: // temporaries

    using tupleCodewords = typename
    mpl::copy <
        std::tuple<>,
        mpl::back_inserter <Codewords>
    >::type;

    using tree = typename
    NodeFactory <
        typename mpl::sort <
            tupleCodewords,
            SortPredicate<mpl::_,mpl::_>
        >::type
    >::type;

    using codewordEngine =
    CodeGenerator <
        tree,
        std::tuple<>
    >;

public:
    using type = typename
    CodeExtractor <
        codewordEngine,
        tree
    >::type;

    static void print(std::ostream& str)
    {
        CcwPrinter<type>::print(str);
    }
};

#define PRINT_HUFFMAN_CODE(SYMBOL_LIST, STREAM) \
HuffmanTree<SYMBOL_LIST>::print(STREAM)

#endif // HUFFMAN_HPP_INCLUDED

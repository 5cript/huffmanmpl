#ifndef SYMBOLCOUNTER_H_INCLUDED
#define SYMBOLCOUNTER_H_INCLUDED

/** This file cannot be used alone and is intended to be included elsewhere internally.
 *  The seperation only serves to lighten up the code to preserve my sanity.
 */

#include "mplincludes.hpp"
#include <tuple>

// this counts in the std::tuple
template <typename SymbolEx, typename Symbol>
struct TupleWrapCounter
{
    template <typename>
    struct TuplePopFront
    {
    };

    template <typename T, typename ... List>
    struct TuplePopFront<std::tuple<T, List...>>
    {
        using type = std::tuple <List...>;
    };

    template <char I_Symbol, class TupleT>
    struct TupleCounterLambda
    {
        using next_ = typename
        TupleCounterLambda<I_Symbol, typename TuplePopFront<TupleT>::type>::type;

        using type = typename
        mpl::if_c <I_Symbol == std::tuple_element<0, TupleT>::type::value,
                   mpl::plus <next_, mpl::int_<1>>,
                   next_>::type;
    };

    template <char I_Symbol>
    struct TupleCounterLambda <I_Symbol, std::tuple<>>
    {
        using type = mpl::int_<0>::type;
    };

    using type = typename
    TupleCounterLambda<Symbol::value, typename SymbolEx::type>::type;
};

template <typename SymbolEx>
class SymbolCounter
{
private:
    using _ = mpl::_;

public:
    using type = typename
    mpl::transform <typename SymbolEx::SymbolTable::type, TupleWrapCounter<SymbolEx, _> >::type;
};

#endif // SYMBOLCOUNTER_H_INCLUDED

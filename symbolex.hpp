#ifndef SYMBOLEX_H_INCLUDED
#define SYMBOLEX_H_INCLUDED

/** This file cannot be used alone and is intended to be included elsewhere internally.
 *  The seperation only serves to lighten up the code to preserve my sanity.
 */

#include "mplincludes.hpp"

// creates a mpl::vector_c of all symbols in List...
template <char C, char ... List>
struct SymbolExtractor
{
    typedef typename
    mpl::if_ <typename mpl::contains <typename SymbolExtractor <List...>::type, mpl::int_<C>>::type,
              typename SymbolExtractor <List...>::type,
              typename mpl::push_back <typename SymbolExtractor <List...>::type, mpl::int_<C>>::type>::type type;
};

#define SYMBOL_EXCEPTION_STRIPPER(SYMBOL)                       \
template <char ... List>                                        \
struct SymbolExtractor <SYMBOL, List...>                        \
{                                                               \
    typedef typename SymbolExtractor <List...>::type type;      \
}

// those chars will be excluded from the encoding process
SYMBOL_EXCEPTION_STRIPPER(' ');
SYMBOL_EXCEPTION_STRIPPER('\r');
SYMBOL_EXCEPTION_STRIPPER('\n');
SYMBOL_EXCEPTION_STRIPPER('\t');
SYMBOL_EXCEPTION_STRIPPER('\a');
SYMBOL_EXCEPTION_STRIPPER('\0');

#undef SYMBOL_EXCEPTION_STRIPPER

// Recursion Terminator
template <>
struct SymbolExtractor <'\0'>
{
    using type = std::tuple<>;
};

// wraps a const char* into an tuple
template <char ... List>
struct TextWrapper
{
    // mpl vectors are too small to fit the complete text.
    // Therefore I will wrap all characters into a std::tuple
    using type = typename std::tuple <mpl::int_<List>...>;

    // this wrapping conservers the 'type' naming-convention. (which I find to be very important in Cpp TMP)
    // it also improves readability and improves the meta interface cleanlyness.
    struct SymbolTable {
        using type = typename SymbolExtractor<List...>::type;
    };

    // may be used later by the user to refer to the original string
    static constexpr char const str[] = { List... };
};

#endif // SYMBOLEX_H_INCLUDED
